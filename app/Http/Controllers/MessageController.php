<?php

namespace App\Http\Controllers;

use App\Events\EventName;
use App\Message;
use Illuminate\Http\Request;
use Symfony\Component\EventDispatcher\Event;

class MessageController extends Controller
{
    public function index()
    {
        return view('chat')->withServerIp(request()->server('SERVER_ADDR'));
    }

    public function data()
    {
        $messages = Message::select('sender','message')
            ->orderBy('created_at','DESC')
            ->limit(30)
            ->get()
            ->toArray();

        return $messages;
    }

    public function store(Request $request)
    {
        
        Message::create($request->all());
        event(new EventName($request->all()));
        
//        event(new App\Events\EventName($request->all())); via routes
    }
}
