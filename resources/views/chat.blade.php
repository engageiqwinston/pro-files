@extends('layouts.master')

@section('content')
        <div class="row" id="app">
            <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12">
                <input class="form-control" type="text" id="sender" v-model="sender">

                <textarea rows="3" class="form-control" type="text" id="text" v-model="message" @keyup.enter="sendChat"></textarea>
                <button class="form-control btn btn-primary" id="fire_me" @click="sendChat"> SEND MESSAGE </button>
            </div>
            <hr>
            <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12">
                <div class="row">
                    <div class="col-md-12" v-for="message in messages">
                        <strong>@{{ message.sender }}</strong> : @{{ message.message  }}
                    </div>
                </div>
            </div>
        </div>
@stop

@section('footer')
    <script src="https://cdn.socket.io/socket.io-1.3.5.js"></script>

    <script>
        var socket = io('http://chat.pro-files.xyz:3000');
        var d = new Date();

        const user_unique_id = d.getTime();

        const app = new Vue({
            el: '#app',
            data : {
                sender : 'Anonymous',
                message : null,
                messages : [],
                id : user_unique_id
            },
            mounted(){
                this.initialize();
            },
            methods :
            {
                initialize(){
                    let self = this;

                    $.get('/message/history',function(response){
                        if(response.length>0)
                        {
                            self.messages = response;
                        }
                    });

                    socket.on("test-channel:App\\Events\\EventName", function(message){

                        if(message.data.id!=self.id)
                        {
                            self.messagePusher(message.data);
                        }
                    });

                },
                sendChat()
                {
                    if(this.message==null || this.message=='')
                    {
                        return false;
                    }

                    var data = {
                        id : this.id,
                        sender :  this.sender,
                        message : this.message,
                        _token: $('meta[name="csrf-token"]').attr('content'),
                    };

                    this.messagePusher(data);
                    this.message = null;

                    $.post('/message',data,function(response){
                        console.log(response);
                    });

                },

                messagePusher(information){
                    this.messages.unshift({
                        sender:information.sender,
                        message:information.message
                    });
                }
            }
        });
    </script>
@stop