<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/','MessageController@index');

Route::get('message/history','MessageController@data');

Route::post('message', 'MessageController@store');

Route::get('test', function () {
    // this checks for the event
    return view('test');
});


Auth::routes();

Route::get('/home', 'HomeController@index');
